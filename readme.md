# newman-reporter-execution-lite
Newman reporter which mimics the built-in JSON reporter but only includes a few key items in the report.

## Install

> The installation should be global if newman is installed globally, local otherwise.

For `global` install:

```console
npm install -g npm i newman-reporter-execution-lite
```

For `local` install:

```console
npm install -S npm i newman-reporter-execution-lite
```


## Usage

In order to enable this reporter, specify `execution-lite` in Newman's `-r` or `--reporters` option , and `--reporter-json-export` for json file export  .

```console
newman run https://www.getpostman.com/collections/631643-f695cab7-6878-eb55-7943-ad88e1ccfd65-JsLv -r execution-lite --reporter-json-export test1.json
```

### Options

#### With Newman CLI 
**futur**

## Compatibility

| **newman-reporter-htmlextra** | **newman** | **node** |
|:------------------------:|:----------:|:--------:|
|         >= v1.1.0          | >= v4.1.0  | >= v8.x  |
The report object contains the following items:  

## Result

** Execution object ***  

Schema followed by the reporter:  
```json
[
   {            
            "url" : "..url..",
            "methode" : "..methode..",
            "status" : "..status..",
            "code" : "..code..",
            "data" : "..data..",
            "reponseTime" : "..time..",
            "assertion" : "..assertion.."
    } , 
   {            
            "url" : "..url..",
            "methode" : "..methode..",
            "status" : "..status..",
            "code" : "..code..",
            "data" : "..data..",
            "reponseTime" : "..time..",
            "assertion" : "..assertion.."
    }  ,
   {            
            "url" : "..url..",
            "methode" : "..methode..",
            "status" : "..status..",
            "code" : "..code..",
            "data" : "..data..",
            "reponseTime" : "..time..",
            "assertion" : "..assertion.."
    }  ,
    ...
]
```

## License

This software is licensed under Apache-2.0. Copyright Postdot Technologies, Inc. See the [LICENSE.md](LICENSE.md) file for more information.


## Special mention

This work have been hugely inspired and copied several aspects of the great work done by 
[newman-reporter-json-run-only](https://www.npmjs.com/package/newman-reporter-json-run-only)  --XO--
[newman-reporter-htmlextra](https://www.npmjs.com/package/newman-reporter-htmlextra) 