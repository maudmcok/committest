var _ = require('lodash');

/**
 * Reporter that simply dumps the custum summary object Run->Execution  to file (default: newman-run-report.json).
 *
 * @param {Object} newman - The collection run object, with event hooks for reporting run details.
 * @param {Object} options - A set of collection run options.
 * @param {String} options.export - The path to which the summary object must be written.
 * @returns {*}
 */

function createSmallSummary(summary) {
    var jsonObj = JSON.parse(JSON.stringify(_.pick(summary, ['run'])));
    var finalAux = [] ;
    var myAux = jsonObj['run']['executions'] ;
    
    for(var index in myAux){
        // Format Url
        var url = myAux[index].request.url.protocol + "://" ;
        myAux[index].request.url.host.forEach(element => {
            url += element + "." ;
        });

        url = url.substr( 0, url.length-1 );

        myAux[index].request.url.path.forEach(element => {
            url += "/" + element  ;
        });
        var status = "Undefined";
        var code =  "Undefined";
        var time = "Undefined";
        var method = "Undefined";
        var assertion = "Undefined";
        //Get element Execution
        if(myAux[index].response !== null && myAux[index].response !== "undefined"){
             status = myAux[index].response.status;
             code =  myAux[index].response.code;
             time = myAux[index].response.responseTime;
        }
        method = myAux[index].request.method ;
        assertion = myAux[index].assertions ;
        if(assertion["error"] =! null )
        var data = ""; 

        for(var aux in assertion){
            if(assertion[aux].hasOwnProperty("error")){
                try {
                    const buf0 = Buffer.from(myAux[index].response.stream.data);
                   // const bufer0 = myAux[index].response.stream.data;// response.stream is a buffer to string
                    data = buf0.toString('utf8');
                    break;
                } catch (e) {
                    data = " - XD - ";
                    break;
                }
            }
        }
       
        var execution = {
            "url" : url,
            "method" : method,
            "status" : status,
            "code" : code,
            "data" : data,
            "reponseTime" : time,
            "assertions" : assertion
        }
        finalAux.push(execution) ; 
    }
    return finalAux;
}
module.exports = function (newman, options) {
    newman.on('beforeDone', function (err, o) {
        if (err) { return; }

        newman.exports.push({
            name: 'newman-reporter-execution-lite',
            default: 'newman-run-report.json',
            path: options.jsonExport,
            content: JSON.stringify(createSmallSummary(o.summary))
        });
    });
};